﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static WRLDSUtils; 

public class WRLDSTrampolinePlugin : WRLDSBasePlugin
{

#if UNITY_ANDROID
    private AndroidJumpHeightCallback onJumpHeightCallback;
    private AndroidJumpRotationCallback onJumpRotationCallback;
    private AndroidJumpTrickCallback onJumpTrickCallback;
#endif

    /// <summary>
    /// SDK Initialization is done in #Awake() 
    /// <code>activity</code>The SDK needs to be instantiated with a reference to the activity 
    /// </summary>
    void Awake()
    {

#if UNITY_ANDROID
        DontDestroyOnLoad(this.gameObject);

        SDK = new AndroidJavaObject("com.wrlds.api.TrampolineWearableProvider", new object[] { GetAndroidUnityPlayerActivity() });
        bleDeviceProvider = SDK.Get<AndroidJavaObject>("bleDeviceProvider"); 
        userProvider = SDK.Get<AndroidJavaObject>("userAccountManager");

        Debug.Log($"SDK initialized -> product type: Trampoline");
#else
        Destroy(this);
#endif
    }

    private void Start()
    {
#if UNITY_ANDROID
        SDK.Call("onStart");
#endif
    }

    void OnApplicationPause(bool pause)
    {
#if UNITY_ANDROID
        if (!pause)
        {
            if (SDK != null)
                SDK.Call("onStart");
        }
        if (pause)
        {
            if (SDK != null)
                SDK.Call("onStop");
        }
#endif
    }

    void OnDestroy()
    {
#if UNITY_ANDROID
        SDK.Call("onDestroy");
#endif
    }

#if UNITY_ANDROID
    public void SetJumpTriggerThreshold(float jumpThreshold)
    {
        SDK.CallStatic("setJumpTriggerThreshold", jumpThreshold);
    }
#endif

#if UNITY_ANDROID
    public float GetJumpTriggerThreshold()
    {
       return SDK.CallStatic<float>("getJumpTriggerThreshold");
    }
#endif

#if UNITY_ANDROID
    public void SetJumpHeightMode(JumpHeightMode jumpHeightMode)
    {

        SDK.CallStatic("setJumpHeightMode", (int)jumpHeightMode);
    }
#endif

#if UNITY_ANDROID
    public WRLDSUtils.JumpHeightMode GetJumpHeightMode()
    {
        return (JumpHeightMode) SDK.CallStatic<int>("getJumpHeightMode");
        
    }
#endif

#if UNITY_ANDROID
    public void JumpHeightEvents(bool enable, AndroidJumpHeightCallback.JumpHeightDelegate dataCallback)
    {
        if (onJumpHeightCallback == null)
            onJumpHeightCallback = new AndroidJumpHeightCallback();

        if (enable)
            onJumpHeightCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onJumpHeightCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public void JumpRotationEvents(bool enable, AndroidJumpRotationCallback.JumpRotationDelegate dataCallback)
    {
        if (onJumpRotationCallback == null)
            onJumpRotationCallback = new AndroidJumpRotationCallback();

        if (enable)
            onJumpRotationCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
        else { onJumpRotationCallback.RemoveSubscriber(dataCallback); }
    }
#endif

#if UNITY_ANDROID
    public void JumpTrickEvents(AndroidJumpTrickCallback.JumpTrickDelegate dataCallback)
    {
        if (onJumpTrickCallback == null)
            onJumpTrickCallback = new AndroidJumpTrickCallback();

        onJumpTrickCallback.AddSubscriber(dataCallback);   //Add the Callback supplied in the parameter to the Callback pub/sub list 
    }
#endif

#if UNITY_ANDROID
    public class AndroidJumpHeightCallback : AndroidJavaProxy
    {
        public delegate void JumpHeightDelegate(BluetoothDevice device, float jumpHeight);
        public event JumpHeightDelegate OnJumpHeightReceived;

        public void AddSubscriber(JumpHeightDelegate callback) { OnJumpHeightReceived += callback; }
        public void RemoveSubscriber(JumpHeightDelegate callback) { OnJumpHeightReceived -= callback; }

        public AndroidJumpHeightCallback() : base("com.wrlds.api.listeners.trampoline.JumpHeightListener")
        {
            WRLDS.SDK.Call("setJumpHeightListener", this);    //Set the needed Callbacks that get data from Android Listeners  
        }

        // This is a callback method defined in the SDK, which is invoked when data is received. 
        void onJumpHeightReceived(AndroidJavaObject device, float jumpHeight)
        {
            if (OnJumpHeightReceived != null)
                OnJumpHeightReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), jumpHeight);

            device.Dispose();
        }

    }

    public class AndroidJumpRotationCallback : AndroidJavaProxy
    {
        public delegate void JumpRotationDelegate(BluetoothDevice device, float[] jumpRotation);
        public event JumpRotationDelegate OnJumpRotationReceived;

        public void AddSubscriber(JumpRotationDelegate callback) { OnJumpRotationReceived += callback; }
        public void RemoveSubscriber(JumpRotationDelegate callback) { OnJumpRotationReceived -= callback; }

        public AndroidJumpRotationCallback() : base("com.wrlds.api.listeners.trampoline.JumpRotationListener")
        {
            WRLDS.SDK.Call("setJumpRotationListener", this);    //Set the needed Callbacks that get data from Android Listeners  
        }

        // This is a callback method defined in the SDK, which is invoked when data is received. 
        void onJumpRotationReceived(AndroidJavaObject device, AndroidJavaObject jumpRotation)
        {
            if (OnJumpRotationReceived != null)
                OnJumpRotationReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), WRLDSUtils.ConvertObjectToFloatArray(jumpRotation));

            device.Dispose();
        }
    }

    public class AndroidJumpTrickCallback : AndroidJavaProxy
    {
        public delegate void JumpTrickDelegate(BluetoothDevice device, Trick jumpTrick);
        public event JumpTrickDelegate OnJumpTrickReceived;

        public void AddSubscriber(JumpTrickDelegate callback) { OnJumpTrickReceived += callback; }
        public void RemoveSubscriber(JumpTrickDelegate callback) { OnJumpTrickReceived -= callback; }

        public AndroidJumpTrickCallback() : base("com.wrlds.api.listeners.trampoline.JumpTrickListener")
        {
            WRLDS.SDK.Call("setJumpTrickListener", this);    //Set the needed Callbacks that get data from Android Listeners  
        }

        // This is a callback method defined in the SDK, which is invoked when data is received. 
        void onJumpTrickReceived(AndroidJavaObject device, int jumpTrick)
        {
            if (OnJumpTrickReceived != null)
                OnJumpTrickReceived.Invoke(WRLDSUtils.GetJavaBluetoothClass(device), (Trick)jumpTrick);

            device.Dispose();
        }
    }
#endif

}
