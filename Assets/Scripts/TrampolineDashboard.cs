﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;
using static WRLDSUtils;
using System;

/**
 * A script that shows 4 data overviews of a 2-player Trampoline game 
 * Games include: 
 * - JumpCount
 * - JumpHeight
 * - JumpRotation
 * - JumpTricks
 */
public class TrampolineDashboard : Singleton<WRLDSTrampolinePlugin>
{
    [SerializeField]
    private int playerCount;
    public int PlayerAmount
    {
        get { return playerCount; }
        set { initializePlayerArrayLength(value); }
    }

    /**
     * Screens/Panels for each part of the Game
     **/
    public GameObject entryScreen, settingsScreen, gameScreen, navBar;
    public Text GameTitle;

    /**
     * UI buttons to navigate to different parts of the game
     **/
    public Button jumpScreenButton, heightScreenButton, rotationScreenButton, trickScreenButton;
    public Button[] previousButton = new Button[5];
    public Button[] settingsButton = new Button[4];

    public Sprite[] dots = new Sprite[4];
    public SpriteRenderer spriteRenderer = new SpriteRenderer();


    public Image[] radialProgressX, radialProgressY, radialProgressZ;
    public Text[] jumpCountText, jumpHeightText, maxJumpHeightText, trickText, rotationXText, rotationYText, rotationZText;
    public Text[] frontFlipText, backFlipText, rightSpinText, leftSpinText, rightFlipText, leftFlipText;
    public Button switchApp, nextButton;

    /**
     * Array of bar transform objects for each user. The bar scales based on the height of the jump
     **/
    public Transform[] heightBar;
    public Transform[] maxHeightBar;
    private float[] maxHeight;
    public Slider jumpSensitivitySlider;
    public Slider jumpHeightModeSwitch;
    public Text currentJumpSensitivity;

    /**
     * Bluetooth device address data store
     **/
    private List<string> deviceAddressStore = new List<string>();

    /**
     * Gamified data stores 
     **/
    float[] totalRotationX, totalRotationY, totalRotationZ;
    int[] jumpCountStore;
    float[] jumpHeightStore;
    float[] previousJumpHeightStore;
    float[][] jumpRotationStore;
    int[][] jumpTrickStore;

    /**
     * Conditions for updating the data  
     **/
    private bool updateJumpCount, updateJumpHeight, updateJumpRotation, updateJumpTrick = false;


    private string screen;
    private List<string> navigationHistory = new List<string>();

    private int count = 0;
    private float smooth = 15.0f;
    private int rotationDataCount = 0;

    private float jumpHeightModeSwitchValue = 0; 

    private string axis = "roll";

    private void Awake()
    {
        WRLDS.Init();
    }
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        Application.targetFrameRate = 60;

        //WRLDS.OnMotionDataReceived += MotionHandler;
        switchApp.onClick.AddListener(OnSwitchAppClick);
        nextButton.onClick.AddListener(OnNextClick);
        jumpScreenButton.onClick.AddListener(OnJumpScreenButtonClick);
        heightScreenButton.onClick.AddListener(OnHeightScreenButtonClick);
        rotationScreenButton.onClick.AddListener(OnRotationScreenButtonClick);
        trickScreenButton.onClick.AddListener(OnTrickScreenButtonClick);

        foreach (Button button in previousButton)
        {
            button.onClick.AddListener(OnPreviousScreenClick);
        }

        foreach (Button button in settingsButton)
        {
            button.onClick.AddListener(OnSettingsScreenButtonClick);
        }

        foreach (Transform bar in heightBar)
        {
            bar.localScale = new Vector3(4f, 1.5f);
        }

        foreach (Transform bar in maxHeightBar)
        {
            bar.localScale = new Vector3(4f, 2f);
        }

        jumpSensitivitySlider.value = WRLDS.GetJumpTriggerThreshold();
        jumpSensitivitySlider.maxValue = 78f;
        jumpSensitivitySlider.onValueChanged.AddListener(OnJumpSliderChanged);

        jumpHeightModeSwitch.maxValue = 1f;
        jumpHeightModeSwitch.value = (int) WRLDS.GetJumpHeightMode();
        jumpHeightModeSwitch.onValueChanged.AddListener(OnJumpHeightModeChanged);

        spriteRenderer.sprite = dots[0];


        Swipe.OnScreenSwiped += OnChangeScreenHandler;

        /// You can subscribe in the code to callbacks from the SDK using lambdas
        WRLDS.JumpHeightEvents(true, (BluetoothDevice device, float height) =>
        {
            bool newDeviceAddress = CheckForNewDeviceAddress(device.getDeviceAddress());

            if (newDeviceAddress)
                initializeDataArrayLength(deviceAddressStore.Count);

            //Attribute the data to a new or existing player
            for (int i = 0; i < deviceAddressStore.Count; i++)
            {
                if (deviceAddressStore[i].Equals(device.getDeviceAddress()))
                {
                    jumpCountStore[i] = jumpCountStore[i] + 1;
                    jumpHeightStore[i] = height;

                    if (jumpCountStore.Length > playerCount || jumpHeightStore.Length > playerCount)
                    {
                        Debug.LogError("The amount of sensors exceeds the amount of players to attribute the data to.");
                        break;
                    }
                    updateJumpCount = true;
                    updateJumpHeight = true;
                }
            }

        });

        WRLDS.JumpRotationEvents(true, (BluetoothDevice device, float[] rotation) =>
        {
            bool newDeviceAddress = CheckForNewDeviceAddress(device.getDeviceAddress());

            if (newDeviceAddress)
                initializeDataArrayLength(deviceAddressStore.Count);

            //Attribute the data to a new or existing player
            for (int i = 0; i < deviceAddressStore.Count; i++)
            {
                if (deviceAddressStore[i].Equals(device.getDeviceAddress()))
                {

                    jumpRotationStore[i] = rotation;

                    if (jumpRotationStore.Length > playerCount)
                    {
                        Debug.LogError("The amount of sensors exceeds the amount of players to attribute the data to.");
                        break;
                    }
                    updateJumpRotation = true;
                    rotationDataCount = 120; //display the data for the rotation for 1 second (60hz update rate)

                }
            }

        });

        WRLDS.JumpTrickEvents((BluetoothDevice device, Trick trick) =>
        {
            bool newDeviceAddress = CheckForNewDeviceAddress(device.getDeviceAddress());

            if (newDeviceAddress)
                initializeDataArrayLength(deviceAddressStore.Count);

            //Attribute the data to a new or existing player
            for (int i = 0; i < deviceAddressStore.Count; i++)
            {
                if (deviceAddressStore[i].Equals(device.getDeviceAddress()))
                {
                    //Increment the identified trick in the trick data store
                    jumpTrickStore[i][(int)trick]++;

                    if (jumpTrickStore.Length > playerCount)
                    {
                        Debug.LogError("The amount of sensors exceeds the amount of players to attribute the data to.");
                        break;
                    }
                    updateJumpTrick = true;

                }
            }
        });

        //WRLDS.EventTrampolineHeight(true, (string dataType, float[] motionData) =>
        //{
        //    heightData = motionData;
        //});

        //WRLDSBallPlugin.BLESettings bleSettings = new WRLDSBallPlugin.BLESettings().SetAutoConnect(false)
        //                        .SetConnectionInterval(50)
        //                        .KeepConnectionActiveInBackground(false);

        //WRLDS.BLE.SetAutoConnect(true)
        //        .SetConnectionInterval(20)
        //        .KeepConnectionActiveInBackground(false);

        //Debug.Log(bleSettings.ToString());

        screen = "entryScreen";
        navigationHistory.Add("");

#endif
    }

    private void Update()
    {
        if (rotationDataCount > 0)
            rotationDataCount--;
        else
        {
            for (int i = 0; i < playerCount; i++)
            {
                rotationXText[i].text = "X: ";
                rotationYText[i].text = "Y: ";
                rotationZText[i].text = "Z: ";

                radialProgressX[i].fillAmount = 0f;
                radialProgressY[i].fillAmount = 0f;
                radialProgressZ[i].fillAmount = 0f;
            }
        }

        if (screen != navigationHistory[navigationHistory.Count -1])
        {
            ScreenSelector(screen);
            navigationHistory.Add(screen);
        }
    }

    private void FixedUpdate()
    {
        if (updateJumpCount)
        {
            for (int i = 0; i < jumpCountStore.Length; i++)
            {
                jumpCountText[i].text = jumpCountStore[i].ToString();
            }

            updateJumpCount = false;
        }

        if (updateJumpHeight)
        {
            for (int i = 0; i < jumpHeightStore.Length; i++)
            {
                if (jumpHeightStore[i] > 1.5f)
                {
                    System.Random random = new System.Random();
                    jumpHeightStore[i] = 1.5f + (random.Next(-5, 5) / 100);
                }

                jumpHeightText[i].text = Math.Round(jumpHeightStore[i]*100).ToString(); //System.Math.Round(Mathf.Lerp(previousHeightData[i], jumpHeightStore[i], 3f), 2).ToString()
                heightBar[i].localScale = new Vector3(4f, Mathf.Lerp(previousJumpHeightStore[i], jumpHeightStore[i] * 2.5f, 1f)) ; 


                if (jumpHeightStore[i] > maxHeight[i])
                {
                    maxJumpHeightText[i].text = Math.Round(jumpHeightStore[i] * 100).ToString(); 
                    maxHeightBar[i].localScale = new Vector3(4f, jumpHeightStore[i] * 2.5f);
                    maxHeight[i] = (jumpHeightStore[i]);
                }

                previousJumpHeightStore[i] = jumpHeightStore[i];

            }

            updateJumpHeight = false;
        }
        if (updateJumpRotation)
        {
            for (int i = 0; i < jumpRotationStore.Length; i++)
            {
                totalRotationX[i] = jumpRotationStore[i][0];
                totalRotationY[i] = jumpRotationStore[i][1];
                totalRotationZ[i] = jumpRotationStore[i][2];

                float x = jumpRotationStore[i][0];
                float y = jumpRotationStore[i][1];
                float z = jumpRotationStore[i][2];

                rotationXText[i].text = "X: " + ((int)x).ToString() + " °";
                rotationYText[i].text = "Y: " + ((int)y).ToString() + " °";
                rotationZText[i].text = "Z: " + ((int)z).ToString() + " °";

                if (Mathf.Sign(x) == -1) radialProgressX[i].fillClockwise = false;
                else { radialProgressX[i].fillClockwise = true; }
                if (Mathf.Sign(y) == -1) radialProgressY[i].fillClockwise = false;
                else { radialProgressY[i].fillClockwise = true; }
                if (Mathf.Sign(z) == -1) radialProgressZ[i].fillClockwise = false;
                else { radialProgressZ[i].fillClockwise = true; }

                radialProgressX[i].fillAmount = Mathf.Abs(x / 360f);
                radialProgressY[i].fillAmount = Mathf.Abs(y / 360f);
                radialProgressZ[i].fillAmount = Mathf.Abs(z / 360f);

            }
            updateJumpRotation = false;
        }
        if (updateJumpTrick)
        {
            for (int i = 0; i < jumpTrickStore.Length; i++)
            {
                //orientation for this setup is the chip on the ankle with charging port facing downwards and pcb components facing outward
                frontFlipText[i].text = jumpTrickStore[i][1].ToString();
                backFlipText[i].text =  jumpTrickStore[i][2].ToString();
                rightSpinText[i].text = jumpTrickStore[i][3].ToString();
                leftSpinText[i].text =  jumpTrickStore[i][4].ToString();
                rightFlipText[i].text = jumpTrickStore[i][5].ToString();
                leftFlipText[i].text =  jumpTrickStore[i][6].ToString();
            }
            updateJumpTrick = false;
        }
    }

    bool CheckForNewDeviceAddress(string newDeviceAddress)
    {
        //Check from which sensor the data is coming from
        if (deviceAddressStore.Count == 0)
        {
            deviceAddressStore.Add(newDeviceAddress);
            return true; 
        }
            
        else
        {
            //We set the default value to true because we can only iterate the loop one-by-one value
            bool foundNewDevice = true;

            foreach (string deviceAddress in deviceAddressStore.ToList())
            {
                if (deviceAddress.Equals(newDeviceAddress))
                {
                    foundNewDevice = false;
                }
                    
            }
            if (foundNewDevice == true)
            {
                deviceAddressStore.Add(newDeviceAddress);            
                return true;
            }
        }
        return false;
    }

    void initializePlayerArrayLength(int arraySize)
    {
        radialProgressX = new Image[arraySize];
        radialProgressY = new Image[arraySize];
        radialProgressZ = new Image[arraySize];

        jumpCountText = new Text[arraySize];
        jumpHeightText = new Text[arraySize];
        maxJumpHeightText = new Text[arraySize];
        trickText = new Text[arraySize];
        rotationXText = new Text[arraySize];
        rotationYText = new Text[arraySize];
        rotationZText = new Text[arraySize];

        frontFlipText = new Text[arraySize];
        backFlipText = new Text[arraySize];
        rightSpinText = new Text[arraySize];
        leftSpinText = new Text[arraySize];
        rightFlipText = new Text[arraySize];
        leftFlipText = new Text[arraySize];

    }

    void initializeDataArrayLength (int arraySize)
    {
        totalRotationX = new float[arraySize];
        totalRotationY = new float[arraySize];
        totalRotationZ = new float[arraySize];

        jumpCountStore = new int[arraySize]; // new List<int>(new int[listSize]);
        jumpHeightStore = new float[arraySize];
        previousJumpHeightStore = new float[arraySize];
        maxHeight = new float[arraySize];

        jumpRotationStore = Enumerable
            .Range(0, arraySize)         // requested size
            .Select(i => new float[3])  // each of which is 3 items array of float
            .ToArray();                 // materialized as array

        jumpTrickStore = Enumerable
            .Range(0, arraySize)         // requested size
            .Select(i => new int[7])  // each of which is 4 items array of float
            .ToArray();                 // materialized as array
    }

    void ScreenSelector(string screen)
    {
        GameObject[] panels = { entryScreen, settingsScreen, gameScreen, navBar };
        string[] panelStrings = { "entryScreen", "settingsScreen", "gameScreen", "navBar"};
        for (int i = 0; i < panels.Length; i++)
        {
            if (screen == panelStrings[i])
            {
                panels[i].SetActive(true);
                screen = panelStrings[i];
            }
            else { panels[i].SetActive(false); }
        }

        if (screen == "gameScreen")
            navBar.SetActive(true);
        else { navBar.SetActive(false); }
    }

    private void OnJumpScreenButtonClick()
    {
        screen = "gameScreen";
        OnChangeScreenHandler(1);
        Swipe.SetNewPage(1);
    }

    private void OnHeightScreenButtonClick()
    {
        screen = "gameScreen";
        OnChangeScreenHandler(2);
        Swipe.SetNewPage(2);
    }

    private void OnRotationScreenButtonClick()
    {
        screen = "gameScreen";
        OnChangeScreenHandler(3);
        Swipe.SetNewPage(3);
    }

    private void OnTrickScreenButtonClick()
    {
        screen = "gameScreen";
        OnChangeScreenHandler(4);
        Swipe.SetNewPage(4);
    }

    private void OnPreviousScreenClick()
    {
        if (screen == "settingsScreen")
            screen = navigationHistory[navigationHistory.Count - 2]; 
        else
        {
            screen = "entryScreen";
        }
    }

    private void OnSettingsScreenButtonClick()
    {
        screen = "settingsScreen";  
    }

#if UNITY_ANDROID
    private void OnJumpSliderChanged(float value)
    {
        WRLDS.SetJumpTriggerThreshold(value);
        currentJumpSensitivity.text = (Math.Round(value)).ToString();
    }

    private void OnJumpHeightModeChanged(float value)
    {
        if (value < jumpHeightModeSwitchValue)
        {
            jumpHeightModeSwitch.value = 0;
            WRLDS.SetJumpHeightMode(JumpHeightMode.ACCELERATION);
        }
        else if (value > jumpHeightModeSwitchValue)
        {
            jumpHeightModeSwitch.value = 1;
            WRLDS.SetJumpHeightMode(JumpHeightMode.TIME);
        }

        jumpHeightModeSwitchValue = value;

    }
#endif

    public void OnChangeScreenHandler(int screenNumber)
    {
        spriteRenderer.sprite = dots[screenNumber - 1];
        switch(screenNumber)
        {
            case 1:
                GameTitle.text = "JUMPCOUNT";
                break;
            case 2:
                GameTitle.text = "JUMPHEIGHT";
                break;
            case 3:
                GameTitle.text = "JUMPROTATION";
                break;
            case 4: 
                GameTitle.text = "JUMPTRICKS";
                break;
        }
    }

    private void OnNextClick()
    {
        SceneManager.LoadScene("TrampolineVisualization");
    }
    private void OnSwitchAppClick()
    {
        SceneManager.LoadScene("GolfSwing");
    }
}