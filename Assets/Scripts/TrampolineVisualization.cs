﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;
using static WRLDSUtils;

/**
 * A script that visualizes the orientation of the chip using quaternion data
 */
public class TrampolineVisualization : Singleton<WRLDSTrampolinePlugin>
{
    public Rigidbody pcb_A;
    public Rigidbody pcb_B;

    public Image connectionStatus_A;
    public Image connectionStatus_B;

    private BluetoothState state_A = BluetoothState.DISCONNECTED;
    private BluetoothState state_B = BluetoothState.DISCONNECTED;

    private string firstDevice;
    private string secondDevice;

    private Dictionary<string, float[]> deviceData = new Dictionary< string, float[]>();

    /**
     * Bluetooth device address data store
     **/
    private List<string> deviceAddressStore = new List<string>();

    public Button switchApp, previousButton;
    private float smooth = 15.0f;

    float timer = 0.0f;

    private void Awake()
    {
        WRLDS.Init();
    }
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        previousButton.onClick.AddListener(OnPreviousClick);
        switchApp.onClick.AddListener(OnSwitchAppClick);

        WRLDS.StartQuaternionDataStream(QuaternionDataHandler);

        WRLDS.StartConnectionStateEvents(ConnectionStateHandler);
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (state_A == BluetoothState.CONNECTED)
        {
            connectionStatus_A.GetComponent<Image>().color = Color.green;
        }
        if (state_B == BluetoothState.CONNECTED)
        {
            connectionStatus_B.GetComponent<Image>().color = Color.green;
        }
        if (state_A == BluetoothState.CONNECTING || state_A == BluetoothState.LINK_LOSS)
        {
            timer += Time.deltaTime;
            int duration = Convert.ToInt32(timer % 10);

            if (duration % 2 == 0)
                connectionStatus_A.GetComponent<Image>().color = Color.green;

            else
                connectionStatus_A.GetComponent<Image>().color = Color.white;
        }
        if (state_B == BluetoothState.CONNECTING || state_B == BluetoothState.LINK_LOSS)
        {
            timer += Time.deltaTime;
            int duration = Convert.ToInt32(timer % 10);

            if (duration % 2 == 0)
                connectionStatus_B.GetComponent<Image>().color = Color.green;

            else
                connectionStatus_B.GetComponent<Image>().color = Color.white;
        }
        if (state_A == BluetoothState.DISCONNECTED)
        {
            connectionStatus_A.GetComponent<Image>().color = Color.red;
        }
        if (state_B == BluetoothState.DISCONNECTED)
        {
            connectionStatus_B.GetComponent<Image>().color = Color.red;
        }
    }

    private void FixedUpdate()
    {
        if (deviceData != null)
        {
            foreach (string deviceAddress in deviceData.Keys)
            {
                if (deviceAddress == firstDevice ) // deviceData.ElementAt(0).Key CF:0A:42:1D:BA:7E //"D4:8E:86:6A:80:04"
                {
                    float[] quaternionData = deviceData[deviceAddress];
                    Quaternion pcbRotation = new Quaternion(-quaternionData[1], quaternionData[2], quaternionData[0], -quaternionData[3]);
                    //Quaternion pcbRotation = new Quaternion(-quaternionData[0], quaternionData[2], -quaternionData[1], -quaternionData[3]);

                    //pcb.transform.rotation = pcbRotation;
                    pcb_A.GetComponent<Rigidbody>().transform.localRotation = Quaternion.Slerp(pcb_A.GetComponent<Rigidbody>().transform.localRotation, pcbRotation, Time.deltaTime * smooth);
                }

                if (deviceAddress == secondDevice ) // deviceData.ElementAt(1).Key //"E1:0B:2E:BE:93:51"
                {
                    float[] quaternionData = deviceData[deviceAddress];
                    Quaternion pcbRotation = new Quaternion(-quaternionData[1], quaternionData[2], quaternionData[0], -quaternionData[3]);
                    pcb_B.GetComponent<Rigidbody>().transform.localRotation = Quaternion.Slerp(pcb_B.GetComponent<Rigidbody>().transform.localRotation, pcbRotation, Time.deltaTime * smooth);
                }
            }


        }
    }


#if UNITY_ANDROID
    private void QuaternionDataHandler(BluetoothDevice bluetoothDevice, float[] data)
    {
        if (deviceData.Count == 0)
        {
            deviceData.Add(bluetoothDevice.getDeviceAddress(), data);
            return;
        }

        string newDeviceAddress = bluetoothDevice.getDeviceAddress();
        char[] newDeviceAddressChars = bluetoothDevice.getDeviceAddress().ToCharArray();
        ArrayList deviceMatch = new ArrayList();

        foreach (string storedDeviceAddress in deviceData.Keys)
        {
            char[] storedDeviceChars = storedDeviceAddress.ToCharArray();

            for (int i = 0; i < newDeviceAddressChars.Length; i++)
            {
                if (!newDeviceAddressChars[i].Equals(storedDeviceChars[i]))
                {
                    deviceMatch.Add(false);
                    break;
                }
                else if (i == newDeviceAddressChars.Length - 1)
                {
                    //If the address matches one of the stored addresses, overwrite the value
                    deviceData[storedDeviceAddress] = data;
                    return;
                }

            }

        }

        //Now we know the new address does not match any stored addresses, so we add a new entry in the Dictionary
        deviceData.Add(newDeviceAddress, data);
    }
#endif

#if UNITY_ANDROID
    private void ConnectionStateHandler(BluetoothDevice device, int connectionState)
    {
        bool newDeviceAddress = CheckForNewDeviceAddress(device.getDeviceAddress());

        //todo: Dynamic adding /removing of devices without hardcoding device addresses 
        if (device.getDeviceAddress() == firstDevice) // CF:0A:42:1D:BA:7E //"D4:8E:86:6A:80:04"
        {
            if (connectionState == -1) state_A = BluetoothState.LINK_LOSS;
            else if (connectionState == 0) state_A = BluetoothState.DISCONNECTED;
            else if (connectionState == 1) state_A = BluetoothState.CONNECTED;
            else if (connectionState == 2) state_A = BluetoothState.CONNECTING;
            else if (connectionState == 3) state_A = BluetoothState.DISCONNECTING;

            Debug.Log("ConnectionState A: " + state_A);
        }
        else if (device.getDeviceAddress() == secondDevice ) // "E1:0B:2E:BE:93:51"
        {
            if (connectionState == -1) state_B = BluetoothState.LINK_LOSS;
            else if (connectionState == 0) state_B = BluetoothState.DISCONNECTED;
            else if (connectionState == 1) state_B = BluetoothState.CONNECTED;
            else if (connectionState == 2) state_B = BluetoothState.CONNECTING;
            else if (connectionState == 3) state_B = BluetoothState.DISCONNECTING;

            Debug.Log("ConnectionState B: " + state_B);
        }

    }
#endif

    bool CheckForNewDeviceAddress(string newDeviceAddress)
    {
        //Check from which sensor the data is coming from
        if (deviceAddressStore.Count == 0)
        {
            deviceAddressStore.Add(newDeviceAddress);
            firstDevice = newDeviceAddress;
            return true;
        }

        else
        {
            //We set the default value to true because we can only iterate the loop one-by-one value
            bool foundNewDevice = true;

            foreach (string deviceAddress in deviceAddressStore.ToList())
            {
                if (deviceAddress.Equals(newDeviceAddress))
                {
                    foundNewDevice = false;
                }

            }
            if (foundNewDevice == true)
            {
                deviceAddressStore.Add(newDeviceAddress);
                if (secondDevice == null)
                    secondDevice = newDeviceAddress;
                else
                {
                    firstDevice = newDeviceAddress;
                }
                return true;
            }
        }
        return false;
    }

    private void OnPreviousClick()
    {
        SceneManager.LoadScene("TrampolineDashboard");
    }

    private void OnSwitchAppClick()
    {
        SceneManager.LoadScene("GolfSwing");
    }

    private void OnScan()
    {
        WRLDS.ScanForDevices();
    }

    private void OnDestroy()
    {
#if UNITY_ANDROID
        WRLDS.StopQuaternionDataStream(QuaternionDataHandler);
        WRLDS.StopConnectionStateEvents(ConnectionStateHandler);
#endif
    }

}
