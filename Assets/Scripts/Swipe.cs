﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/**
 * Based on the script by -> Waldo
 * https://pressstart.vip/tutorials/2019/05/15/95/swiping-pages-in-unity.html
**/

public class Swipe : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public  delegate void ScreenSwipeDelegate(int screenNumber);
    public static event ScreenSwipeDelegate OnScreenSwiped; 

    private static Vector3 panelLocation;
    private static int currentPage = 1;
    private static bool newPageSet;

    public float percentThreshold = 0.2f;
    public float easing = 0.5f;
    public int totalPages = 4;


    void Start()
    {
        panelLocation = transform.position;
    }


    public void OnDrag(PointerEventData data)
    {
        float difference = data.pressPosition.x - data.position.x;
        transform.position = panelLocation - new Vector3(difference / 2, 0, 0);
    }


    public void OnEndDrag(PointerEventData data)
    {
        float percentage = (data.pressPosition.x - data.position.x) / Screen.width;
        if (Mathf.Abs(percentage) >= percentThreshold)
        {
            Vector3 newLocation = panelLocation;
            if (percentage > 0 && currentPage < totalPages)
            {
                currentPage++;
                newLocation += new Vector3(-Screen.width/1.5f, 0, 0);
            }
            else if (percentage < 0 && currentPage > 1)
            {
                currentPage--;
                newLocation += new Vector3(Screen.width/1.5f, 0, 0);
            }
            StartCoroutine(SmoothMove(transform.position, newLocation, easing));
            panelLocation = newLocation;
        }
        else
        {
            StartCoroutine(SmoothMove(transform.position, panelLocation, easing));
        }
        if (OnScreenSwiped != null)
            OnScreenSwiped.Invoke(currentPage);

    }


    IEnumerator SmoothMove(Vector3 startpos, Vector3 endpos, float seconds)
    {
        float t = 0f;
        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            transform.position = Vector3.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    IEnumerator ImmediateMove(Vector3 startpos, Vector3 endpos)
    {
        transform.position = endpos;
        yield return null;
    }

    void Update()
    {
        if (newPageSet)
        {
            StartCoroutine(ImmediateMove(transform.position, panelLocation));
            newPageSet = false;
        }
    }

    public static void SetNewPage(int nextPage)
    {
        switch (nextPage)
        {
            case 1:
                switch(currentPage)
                {
                    case 1:
                        // do nothing
                        break;
                    case 2:
                        panelLocation += new Vector3(Screen.width / 1.5f, 0, 0);
                        break;
                    case 3:
                        panelLocation += new Vector3(Screen.width / 0.75f, 0, 0);
                        break;
                    case 4:
                        panelLocation += new Vector3(Screen.width / 0.5f, 0, 0);
                        break;
                }
                break;
            case 2:
                switch (currentPage)
                {
                    case 1:
                        panelLocation += new Vector3(-Screen.width / 1.5f, 0, 0);
                        break;
                    case 2:
                        // do nothing
                        break;
                    case 3:
                        panelLocation += new Vector3(Screen.width / 1.5f, 0, 0);
                        break;
                    case 4:
                        panelLocation += new Vector3(Screen.width / 0.75f, 0, 0);
                        break;
                }
                break;
            case 3:
                switch (currentPage)
                {
                    case 1:
                        panelLocation += new Vector3(-Screen.width / 0.75f, 0, 0);
                        break;
                    case 2:
                        panelLocation += new Vector3(-Screen.width / 1.5f, 0, 0);
                        break;
                    case 3:
                        // do nothing
                        break;
                    case 4:
                        panelLocation += new Vector3(Screen.width / 1.5f, 0, 0);
                        break;
                }
                break;
            case 4:
                switch (currentPage)
                {
                    case 1:
                        panelLocation += new Vector3(-Screen.width / 0.5f, 0, 0);
                        break;
                    case 2:
                        panelLocation += new Vector3(-Screen.width / 0.75f, 0, 0);
                        break;
                    case 3:
                        panelLocation += new Vector3(-Screen.width / 1.5f, 0, 0);
                        break;
                    case 4:
                        // do nothing
                        break;
                }
                break;
        }

        currentPage = nextPage;
        newPageSet = true;
    }
}
